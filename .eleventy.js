const inputDir = 'input';
const componentsDir = `${inputDir}/_includes/components`;

// Components
// const Card = require(`./${componentsDir}/Card.js`);

module.exports = function (config) {
    
  //Paired shortcodes
  // config.addPairedShortcode('Card', Card);

  // Shortcodes
  // config.addShortcode('PinkBanner', PinkBanner);

  return {
    dir: {
      input: inputDir,
      output: 'public'
    }
  };
};
