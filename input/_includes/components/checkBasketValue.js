function checkBasket(){
  console.log('loaded');

  if (localStorage.getItem('test-event-1') === null) {
    window.localStorage.setItem('test-event-1', 'false');
  }
  if (localStorage.getItem('test-event-2') === null) {
    window.localStorage.setItem('test-event-2', 'false');
  }
  if (localStorage.getItem('test-event-3') === null) {
    window.localStorage.setItem('test-event-3', 'false');
  }

  var itemsInBasket = 0;

  var testEvent1_value = window.localStorage.getItem('test-event-1');
  if (testEvent1_value === 'true'){
      itemsInBasket++;
      console.log("test-event-1 in basket");
  }
  var testEvent2_value = window.localStorage.getItem('test-event-2');
  if (testEvent2_value === 'true'){
    itemsInBasket++;
    console.log("test-event-2 in basket");
  }
  var testEvent3_value = window.localStorage.getItem('test-event-3');
  if (testEvent3_value === 'true'){
    itemsInBasket++;
    console.log("test-event-3 in basket");
  }

  if (document.getElementById("basketItems")!==null){
    document.getElementById("basketItems").textContent=itemsInBasket + " item(s) in basket";
    console.log(itemsInBasket);
  }
  

  var eventNameElement = document.getElementById('eventName');
  if (eventNameElement){
    console.log("there is a head element that we're looking for");
    var courseName = eventNameElement.getAttribute('name');
    console.log(courseName);
    var savedOccurrenceNumber = localStorage.getItem(courseName+"-occurrence");
    if (savedOccurrenceNumber){
      document.getElementById(savedOccurrenceNumber).classList.add("active");
      console.log(localStorage.getItem(courseName+"-occurrence"));
      document.getElementById("addBookingHolder").style.display = "none";

      var unselectedOccurrences = document.getElementsByClassName("sessions");
      for (var j=0; j<unselectedOccurrences.length; j++){
        if (unselectedOccurrences[j].classList.contains('active')){
        } else {
          unselectedOccurrences[j].style.display="none";
        }
      }

      var occurrenceTitles = document.getElementsByClassName("occurrenceTitle");
      for (var y=0; y<occurrenceTitles.length; y++){
        occurrenceTitles[y].style.display="none";
      }

      document.getElementById('subTitleOccurrences').innerText = "In your basket";
      
      displayEditBookings();
    }
  }
}