function priceManager(eventTitle, discount){
  console.log("Price manager ONLINE");
  console.log("Received course code " + eventTitle);
  var standardCoursePrice = "";
  switch(eventTitle) {
    case "test-event-1":
      standardCoursePrice = "220";

      break;
    case "test-event-2":
      standardCoursePrice = "1100";
      break;
    case "test-event-3":
      standardCoursePrice = "440";
      break;
  } 
  var children = document.querySelector('#' + eventTitle).children;
  var coursePrice = children[2].getElementsByClassName('coursePrice');
  if (discount === true){
    coursePrice[0].innerText = "£0";
    console.log(eventTitle+"-discount");
    document.getElementById(eventTitle+"-discount").style.display = "table-row";
    console.log(standardCoursePrice);
    window.localStorage.setItem('voucher', standardCoursePrice);
    window.localStorage.setItem('voucher-event', eventTitle);
  } else {
    coursePrice[0].innerText = "£" + standardCoursePrice;
    console.log(eventTitle+"-discount");
    document.getElementById(eventTitle+"-discount").style.display = "none";
    window.localStorage.setItem('voucher', '0');
    window.localStorage.setItem('voucher-event', "");
  }

  checkBasketItemsToDisplay();
  
  console.log(children);
}