var addedOccurrenceNumber = "";

function selectSessions(sessionNo){
  var activeElements = document.getElementsByClassName("active");
  for (var i=0; i<activeElements.length; i++){
    activeElements[i].classList.remove("active");
  }
  console.log("you want " + sessionNo);
  addedOccurrenceNumber = sessionNo;
  document.getElementById(sessionNo).classList.add("active");
}

function checkForBooking(){
  if ((document.getElementById('eventName')!==null)&&(window.localStorage.getItem('allOrders')!==null)){
    var thisEventCode = document.getElementById('eventName').getAttribute("name");

    var allOrders = JSON.parse(window.localStorage.getItem('allOrders'));
    console.log(allOrders.length);

    var bookedCourses = [];
    for (let i=0; i < allOrders.length; i++){
      var thisItemKeyNames = [];
      thisItemKeyNames = Object.keys(allOrders[i]);
      console.log("this item keyname");
      console.log(thisItemKeyNames);
      for (let j=0; j < thisItemKeyNames.length; j++){
        if (thisItemKeyNames[j].includes("item")){
          console.log(thisItemKeyNames[j]);
          console.log(allOrders[i][thisItemKeyNames[j]].title);
          if (allOrders[i][thisItemKeyNames[j]].title === thisEventCode){
            var savedOccurrenceNumber = allOrders[i][thisItemKeyNames[j]].occurrence;
            selectSessions(savedOccurrenceNumber);
        console.log('Booked for ' + savedOccurrenceNumber );

        document.getElementById("addBookingHolder").style.display = "none";

        var unselectedOccurrences = document.getElementsByClassName("sessions");
        for (let x=0; x<unselectedOccurrences.length; x++){
          if (unselectedOccurrences[x].classList.contains('active')){
          } else {
            unselectedOccurrences[x].style.display="none";
          }
        }

        var occurrenceTitles = document.getElementsByClassName("occurrenceTitle");
        for (var y=0; y<occurrenceTitles.length; y++){
          occurrenceTitles[y].style.display="none";
        }

        document.getElementById('subTitleOccurrences').innerText = "You're already booked onto the following session:";
        document.getElementById('removeBooking').style.display = "block";
          }
        }
      }
    }
  }
}

function removeCourseFromBooking(){
  console.log("---");
  if ((document.getElementById('eventName')!==null)&&(window.localStorage.getItem('allOrders')!==null)){
    var thisEventCode = document.getElementById('eventName').getAttribute("name");

    var allOrders = JSON.parse(window.localStorage.getItem('allOrders'));
    console.log(allOrders);

    var bookedCourses = [];
    for (let i=0; i < allOrders.length; i++){
      var thisItemKeyNames = [];
      thisItemKeyNames = Object.keys(allOrders[i]);
      console.log("this item keyname");
      console.log(thisItemKeyNames);
      for (let j=0; j < thisItemKeyNames.length; j++){
        if (thisItemKeyNames[j].includes("item")){
          console.log(thisItemKeyNames[j]);
          console.log(allOrders[i][thisItemKeyNames[j]].title);
          console.log("---" + thisEventCode);

          if (allOrders[i][thisItemKeyNames[j]].title === thisEventCode){
            console.log("A MATCH");
            if (thisItemKeyNames.length <= 2){
              console.log("length");
              console.log(thisItemKeyNames.length);

              allOrders = allOrders.splice(i-1, 1);
              console.log(i);
              console.log(allOrders);
              break;
            } else {
              console.log("In the else j=" + j );
              console.log(allOrders[i]);
              delete allOrders[i] ["item" + (j+1)];
              console.log(allOrders);
            }

          }
        }
      }
    }
    displayPopUp("info-removingBooking");
    setTimeout(function(){hidePopUps("info-removingBooking");displayPopUp("info-cancelledItemSuccess");}, 3000);
    window.localStorage.setItem("allOrders", JSON.stringify(allOrders));
    resetBooking();
  }
}